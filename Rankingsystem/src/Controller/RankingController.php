<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Ranking Controller
 *
 * @property \App\Model\Table\RankingTable $Ranking
 *
 * @method \App\Model\Entity\Ranking[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RankingController extends AppController
{
    /**
     * Index method
     */
    public function index(){}

    /**
     * View method
     */
    public function view()
    {
        //検索ワードの取得
        $townSearchWord = $this->request->getData('townSearch');
        $reSearchWord = $this->request->getData('reasonSearch');
        //検索ワードでの部分一致検索
        $result = $this->Ranking->find('all',array(
            'conditions' => array('Ranking.Town LIKE' => '%'.$townSearchWord.'%',
                                    'Ranking.Reason LIKE' => '%'.$reSearchWord.'%'),
            'order' => array('PopularityRank ASC'),
            'limit' => 10));
        //検索結果をセット
        $this->set('rankings', $result);
    }
}
