<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Ranking Entity
 *
 * @property int $Id
 * @property int $PopularityRank
 * @property string $Ward
 * @property string $Town
 * @property string $Reason
 */
class Ranking extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'PopularityRank' => true,
        'Ward' => true,
        'Town' => true,
        'Reason' => true
    ];
}
