<?php
echo $this->Html->css('ranking');?>

<table>
<tr>
<td>順位</td>
<td>区名</td>
<td>街名</td>
<td>理由</td>
</tr>
<?php foreach ($rankings as $ranking): ?>
<tr>
<td><?=h($ranking->PopularityRank) ?></td>
<td><?=h($ranking->Ward) ?></td>
<td><?=h($ranking->Town) ?></td>
<td><?=h($ranking->Reason) ?></td>
</tr>
<?php endforeach; ?> 
</table>

<font size="3" color="15848F">
<b>
<?=$this->Form->create('ranking', ['url' => ['action' => 'index'], 'type' => 'post'])?>
<?=$this->Form->submit('戻る')?> <?=$this->Form->end()?>
</b>
</font>