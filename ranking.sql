-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- ホスト: 127.0.0.1
-- 生成日時: 
-- サーバのバージョン： 10.4.6-MariaDB
-- PHP のバージョン: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- データベース: `ranking_schema`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `ranking`
--

CREATE TABLE `ranking` (
  `Id` int(10) NOT NULL,
  `PopularityRank` int(3) NOT NULL,
  `Ward` varchar(100) CHARACTER SET utf8 NOT NULL,
  `Town` varchar(100) CHARACTER SET utf8 NOT NULL,
  `Reason` longtext CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- テーブルのデータのダンプ `ranking`
--

INSERT INTO `ranking` (`Id`, `PopularityRank`, `Ward`, `Town`, `Reason`) VALUES
(1, 1, '目黒区', '中目黒', '高級感あふれる街並みがよい。桜並木が綺麗。お洒落なカフェが多い。'),
(2, 10, '文京区', '本郷', '東京駅、上野駅などの主要ターミナルが近く、交通の便がよい。落ち着いた雰囲気で、治安がよい。'),
(3, 6, '杉並区', '阿佐ヶ谷', '街中に衣食住が整っている。なおかつ住民は女性やファミリーの多いため治安がよい。'),
(4, 9, '渋谷区', '佐々木上原', '渋谷区なのに、街の雰囲気が落ち着いている。ほどよくお洒落。交通の便がよい。'),
(5, 3, '品川区', '武蔵小山', '活気ある商店街が多い。再開発されて街並みが綺麗。'),
(6, 8, '港区', '青山', '整えられた街並みが綺麗。セレブ在住のため洗礼された雰囲気。'),
(7, 2, '北区', '赤羽', '都会すぎず、落ち着いた雰囲気。家庭を持ったときにも融通が利きそう。'),
(8, 4, '江東区', '豊洲', 'お台場海浜公園や豊洲のららぽーと、新市場などの観光地へ気軽に行くことができる。東京駅などへのアクセスがよく、交通の便がよい。'),
(9, 7, '豊島区', '池袋', '駅周辺にはデパートやレジャー施設が多い。駅から離れると病院やスーパーのある閑静な住宅街があるのでよい。'),
(10, 5, '世田谷区', '下北沢', 'サブカル好きの若者にはたまらない街。'),
(11, 11, '渋谷区', '恵比寿', '高級住宅街で上品。');

--
-- ダンプしたテーブルのインデックス
--

--
-- テーブルのインデックス `ranking`
--
ALTER TABLE `ranking`
  ADD PRIMARY KEY (`Id`);

--
-- ダンプしたテーブルのAUTO_INCREMENT
--

--
-- テーブルのAUTO_INCREMENT `ranking`
--
ALTER TABLE `ranking`
  MODIFY `Id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
